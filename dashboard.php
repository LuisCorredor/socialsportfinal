<?php
session_start();
?>

<html>

<head>
    <title>SocialSPorts</title>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
</head>

<body>
    <?php include "php/navbar.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img src="img/backend.jpg" class="fondo">
                        <div class="caption">
                            <h3 class="center">Hola Admin</h3>
                            <p>En este entorno puedes registrar los datos correspondiente a un club</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3>Bienvenido a Social Sports</h3>
                            <p>Bienvenido a su plataforma como administrador, en donde tendrá acceso a registrar, eliminar y editar los datos de un club.</p>
                        </div>
                    </div>
                </div>
            </div>

            <a href="./ver.php">
                <div class="col-md-4" style="margin-top:10px;">
                    <div class="thumbnail">
                        <div class="caption">
                            <center>
                                <img src="img/soccer-field.png" style="height:auto; width: 20% !important;">
                            </center>
                            <p style="text-decoration:none;" align="center">Gestionar Clubes</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</body>

</html>

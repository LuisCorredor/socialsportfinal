<html>

<head>
    <title>SocialSports</title>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
</head>

<body>
    <?php include "php/navbar.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Cargar un equipo</h2>
                <!-- Button trigger modal -->
                <a data-toggle="modal" href="#myModal" class="btn btn-default">Agrega tu equipo</a>
                <br><br>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Agregar equipo</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form" method="post" action="php/agregarequipo.php">
                                    <div class="form-group">
                                        <label for="nombre">Nombre:</label>
                                        <input type="text" class="form-control" name="nombre" required>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="numeroJugadores">Número de jugadores:</label>
                                        <input type="text" class="form-control" name="numeroJugadores" required>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="idUsuario">Usuario:</label>
                                        <input type="text" class="form-control" name="idUsuario" required>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="idPartido">Partido:</label>
                                        <input type="text" class="form-control" name="idPartido" required>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="descripcion">Descripcion:</label>
                                        <input type="text" class="form-control" name="descripcion" required>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="idClub">Club:</label>
                                        <input type="text" class="form-control" name="idClub" required>
                                    </div>
                                    
                                    <button type="submit" class="btn btn-default">Agregar</button>
                                </form>
                            </div>

                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->


                <?php include "php/tablaequipo.php"; ?>
            </div>
        </div>
    </div>

    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>

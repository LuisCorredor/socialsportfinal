<!doctype html>
<html lang="en">

<head>
    <?php include 'layaouts/header.php'; ?>
</head>

<body class="back">
    <div class="container">
        <div class="col-md-8">
            <div class="col-md-6 center card">

                <img src="img/football-ball.png" class="login-img">
                <br>
                <h3 class="text-center title">Social Sport</h3>
                <br>

                <form action="processusers.php" method="post" autocomplete="off" class="col-md-12">
                    <div class="form-group">
                        <label for="nombre">Username:</label>
                        <input id="nombre" type="text" class="form-control" name="nombre" placeholder="Ingresa tu nombre de usuario" required>
                    </div>

                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" name="password" placeholder="Ingresa tu contraseña" id="pass" required>
                    </div>

                    <div class="input-group">
                        <button type="submit" class="btn btn-block btn-primary" name="login_user">Ingresar</button>
                    </div>
                    
                    <div class="input-group">
                        <a class="btn btn-block" href="registeruser.php" style="margin-top:10px;">Registrate</a>
                    </div>
                    
                    <div class="input-group">
                        <a class="btn btn-block" href="login-admin.php" style="margin-top:10px;">Admin</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <video id="video_background" autoplay muted loop>
        <source src="video/Soccer%20field%20-%20Footbull-%20Animated%20background.mp4" type="video/mp4">
    </video>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
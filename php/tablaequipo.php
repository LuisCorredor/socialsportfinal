<?php

include "conexion.php";

$user_id=null;
$sql1= "select * from equipo";
$query = $con->query($sql1);
?>

<?php if($query->num_rows>0):?>
<table class="table table-bordered table-hover">
<thead>
	<th>Id Equipo</th>
	<th>Nombre</th>
	<th>Numero de Jugadores</th>
	<th>Id Usuario</th>
	<th>Id Partido</th>
</thead>
<?php while ($r=$query->fetch_array()):?>
<tr>
	<td><?php echo $r["idEquipo"]; ?></td>
	<td><?php echo $r["nombre"]; ?></td>
	<td><?php echo $r["numeroJugadores"]; ?></td>
	<td><?php echo $r["idUsuario"]; ?></td>
	<td><?php echo $r["idPartido"]; ?></td>
	<td style="width:150px;">
		<a href="./editarequipo.php?id=<?php echo $r["idEquipo"];?>" class="btn btn-sm btn-warning">Editar</a>
		<a href="#" id="del-<?php echo $r["idEquipo"];?>" class="btn btn-sm btn-danger">Eliminar</a>
		<script>
		$("#del-"+<?php echo $r["idEquipo"];?>).click(function(e){
			e.preventDefault();
			p = confirm("Estas seguro?");
			if(p){
				window.location="./php/eliminarequipo.php?id="+<?php echo $r["idEquipo"];?>;

			}

		});
		</script>
	</td>
</tr>
<?php endwhile;?>
</table>
<?php else:?>
	<p class="alert alert-warning">No hay resultados</p>
<?php endif;?>

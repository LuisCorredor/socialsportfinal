<?php

include "conexion.php";

$user_id=null;
$sql1= "select * from calificaciones";
$query = $con->query($sql1);
?>

<?php if($query->num_rows>0):?>
<table class="table table-bordered table-hover">
<thead>
	<th>Texto</th>
	<th>Número de Usuario</th>
	<th></th>
</thead>
<?php while ($r=$query->fetch_array()):?>
<tr>
	<td><?php echo $r["texto"]; ?></td>
	<td><?php echo $r["idUsuario"]; ?></td>
	<td style="width:150px;">
		<a href="./editarcalificacion.php?id=<?php echo $r["idCalificacion"];?>" class="btn btn-sm btn-warning">Editar</a>
		<a href="#" id="del-<?php echo $r["idCalificacion"];?>" class="btn btn-sm btn-danger">Eliminar</a>
		<script>
		$("#del-"+<?php echo $r["idCalificacion"];?>).click(function(e){
			e.preventDefault();
			p = confirm("Estas seguro?");
			if(p){
				window.location="./php/eliminarcalificacion.php?id="+<?php echo $r["idCalificacion"];?>;

			}

		});
		</script>
	</td>
</tr>
<?php endwhile;?>
</table>
<?php else:?>
	<p class="alert alert-warning">No hay calificaciones cargadas a otros usuarios</p>
<?php endif;?>

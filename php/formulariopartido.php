<?php
    include "conexion.php";

    $partido_id=null;
    $sql1= "SELECT * FROM partido WHERE idPartido = ".$_GET["id"];
    $query = $con->query($sql1);
    $partido = null;
    if($query->num_rows>0){
        while ($r=$query->fetch_object()){
            $partido=$r;
        break;
        }
    }
?>

    <?php if($partido!=null):?>

    <form role="form" method="post" action="php/actualizarpartido.php">
        <div class="form-group">
            <label for="status">status</label>
            <input type="text" class="form-control" value="<?php echo $partido->status; ?>" name="status" required>
        </div>
        <div class="form-group">
            <label for="fecha">fecha</label>
            <input type="text" class="form-control" value="<?php echo $partido->fecha; ?>" name="fecha" required>
        </div>
        <div class="form-group">
            <label for="hora">hora</label>
            <input type="text" class="form-control" value="<?php echo $partido->hora; ?>" name="hora" required>
        </div>
        <div class="form-group">
            <label for="titulo">Titulo</label>
            <input type="text" class="form-control" value="<?php echo $partido->titulo; ?>" name="titulo">
        </div>
        <div class="form-group">
            <label for="descripcion">Descripcion</label>
            <input type="text" class="form-control" value="<?php echo $partido->descripcion; ?>" name="descripcion">
        </div>
        
        <div class="form-group">
            <label for="idClub">Nombre del Club</label>
            <input type="text" class="form-control" value="<?php echo $partido->nombreClub; ?>" name="nombreClub">
        </div>
      
        <input type="hidden" name="id" value="<?php echo $partido->idPartido; ?>">
        <button type="submit" class="btn btn-default">Actualizar</button>
    </form>
    <?php else:?>
        <p class="alert alert-danger">404 No se encuentra</p>
    <?php endif;?>

<?php

include "conexion.php";

$user_id=null;
$sql1= "select idClub,nombre,direccion,cantidadCanchas,tipo,disponibilidad from club";
$query = $con->query($sql1);
?>

<?php if($query->num_rows>0):?>
<table class="table table-bordered table-hover">
<thead>
	<th>Id Club</th>
	<th>Nombre</th>
	<th>Direccion</th>
	<th>Cantidad de canchas</th>
	<th>Tipo</th>
	<th>Disponibilidad</th>
	<th></th>
</thead>
<?php while ($r=$query->fetch_array()):?>
<tr>
	<td><?php echo $r["idClub"]; ?></td>
	<td><?php echo $r["nombre"]; ?></td>
	<td><?php echo $r["direccion"]; ?></td>
	<td><?php echo $r["cantidadCanchas"]; ?></td>
	<td><?php echo $r["tipo"]; ?></td>
	<td><?php echo $r["disponibilidad"]; ?></td>
	<td style="width:150px;">
		<a href="./editar.php?id=<?php echo $r["idClub"];?>" class="btn btn-sm btn-warning">Editar</a>
		<a href="#" id="del-<?php echo $r["idClub"];?>" class="btn btn-sm btn-danger">Eliminar</a>
		<script>
		$("#del-"+<?php echo $r["idClub"];?>).click(function(e){
			e.preventDefault();
			p = confirm("Estas seguro?");
			if(p){
				window.location="./php/eliminar.php?id="+<?php echo $r["idClub"];?>;

			}

		});
		</script>
	</td>
</tr>
<?php endwhile;?>
</table>
<?php else:?>
	<p class="alert alert-warning">No hay resultados</p>
<?php endif;?>

<?php

include "conexion.php";

$user_id=null;
$sql1= "select * from partido";
$query = $con->query($sql1);
?>

<?php if($query->num_rows>0):?>
<table class="table table-bordered table-hover">
<thead>
	<th>Id Partido</th>
	<th>Status</th>
	<th>Fecha</th>
	<th>Hora</th>
	<th>Titulo</th>
	<th>Descripcion</th>
	<th>Nombre Club</th>
</thead>
<?php while ($r=$query->fetch_array()):?>
<tr>
	<td><?php echo $r["idPartido"]; ?></td>
	<td><?php echo $r["status"]; ?></td>
	<td><?php echo $r["fecha"]; ?></td>
	<td><?php echo $r["hora"]; ?></td>
	<td><?php echo $r["titulo"]; ?></td>
	<td><?php echo $r["descripcion"]; ?></td>
	<td><?php echo $r["nombreClub"]; ?></td>
	<td style="width:150px;">
		<a href="./editarpartido.php?id=<?php echo $r["idPartido"];?>" class="btn btn-sm btn-warning">Editar</a>
		<a href="#" id="del-<?php echo $r["idPartido"];?>" class="btn btn-sm btn-danger">Eliminar</a>
		<script>
		$("#del-"+<?php echo $r["idPartido"];?>).click(function(e){
			e.preventDefault();
			p = confirm("Estas seguro?");
			if(p){
				window.location="./php/eliminarpartido.php?id="+<?php echo $r["idPartido"];?>;

			}

		});
		</script>
	</td>
</tr>
<?php endwhile;?>
</table>
<?php else:?>
	<p class="alert alert-warning">No hay partidos cargados</p>
<?php endif;?>

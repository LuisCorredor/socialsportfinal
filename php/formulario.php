<?php
    include "conexion.php";

    $user_id=null;
    $sql1= "SELECT idClub,nombre,direccion,cantidadCanchas,tipo,disponibilidad FROM club WHERE idClub = ".$_GET["id"];
    $query = $con->query($sql1);
    $club = null;
    if($query->num_rows>0){
        while ($r=$query->fetch_object()){
            $club=$r;
        break;
        }
    }
?>

    <?php if($club!=null):?>

    <form role="form" method="post" action="php/actualizar.php">
        <div class="form-group">
            <label for="idClub">Id Club</label>
            <input type="text" class="form-control" value="<?php echo $club->idClub; ?>" name="idClub" required>
        </div>
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" value="<?php echo $club->nombre; ?>" name="nombre" required>
        </div>
        <div class="form-group">
            <label for="direccion">Direccion</label>
            <input type="text" class="form-control" value="<?php echo $club->direccion; ?>" name="direccion" required>
        </div>
        <div class="form-group">
            <label for="cantidadCanchas">Cantidad de canchas</label>
            <input type="text" class="form-control" value="<?php echo $club->cantidadCanchas; ?>" name="cantidadCanchas" required>
        </div>
        <div class="form-group">
            <label for="tipo">tipo</label>
            <input type="text" class="form-control" value="<?php echo $club->tipo; ?>" name="tipo" required>
        </div>
        <div class="form-group">
            <label for="disponibilidad">Disponibilidad</label>
            <input type="text" class="form-control" value="<?php echo $club->disponibilidad; ?>" name="disponibilidad">
        </div>
      
        <input type="hidden" name="id" value="<?php echo $club->idClub; ?>">
        <button type="submit" class="btn btn-default">Actualizar</button>
    </form>
    <?php else:?>
        <p class="alert alert-danger">404 No se encuentra</p>
    <?php endif;?>

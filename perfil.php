<?php
session_start();
?>

<html>

<head>
    <title>SocialSPorts</title>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
</head>

<body>
    <?php include "php/navbar.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img src="img/backend.jpg" class="fondo">
                        <div class="caption">
                            <h3 class="center">Hola Jugador</h3>
                            <p>En este entorno puedes Crear tu partido de futbol</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="col-md-12">
                        <div class="thumbnail">
                            <div class="caption">
                                <h3 class="text-center welcome">Bienvenido a Social Sport</h3>
                                <p align="justify">Social Sport te resuelve la partida. En nuestra página web puedes reservar y pagar la cancha de tu preferencia con crédito, transferencia o depósito. Si te quedaste sin panas para jugar fútbol, nuestra aplicación es justo lo que necesitas, miles de jugadores te esperan ahí para armar buenas partidas en las mejores canchas de Argentina.</p>

                                <p align="justify">Descarga nuestra app para que te unas a nuestras partidas públicas en Buenos Aires, La Hora Social Sport. También puedes crear tus propias partidas, ponlas privadas si solo quieres invitar a tus amigos o ponlas públicas si no tienes suficiente gente.</p>

                                <p align="justify">Al finalizar la partida recuerda evaluar a las personas que jugaron contigo, puedes ponerles una calificación por su nivel de juego y por su actitud, así otros verán si es un buen o mal jugador.</p>
                            </div>

                        </div>
                    </div>
                    
                    <a href="./calificacion.php">
                        <div class="col-md-4" style="margin-top:10px;">
                            <div class="thumbnail">
                                <div class="caption">
                                    <center>
                                        <img src="img/review.png" style="height:auto; width: 20% !important;">
                                    </center>
                                    <p style="text-decoration:none;" align="center">Calificar Jugador</p>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="./cargarpartido.php">
                        <div class="col-md-4" style="margin-top:10px;">
                            <div class="thumbnail">
                                <div class="caption">
                                    <center>
                                        <img src="img/soccer-field.png" style="height:auto; width: 20% !important;">
                                    </center>
                                    <p style="text-decoration:none;" align="center">Crear Partido</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    
                    <a href="./equipo.php">
                        <div class="col-md-4" style="margin-top:10px;">
                            <div class="thumbnail">
                                <div class="caption">
                                    <center>
                                        <img src="img/match.png" style="height:auto; width: 20% !important;">
                                    </center>
                                    <p style="text-decoration:none;" align="center">Mis Equipos</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>

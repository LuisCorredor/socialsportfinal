<html>

<head>
    <title>SocialSports</title>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
</head>

<body>
    <?php include "php/navbar.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Cargar un partido</h2>
                <!-- Button trigger modal -->
                <a data-toggle="modal" href="#myModal" class="btn btn-default">Agregar Partido</a>
                <br><br>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Agregar partido</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form" method="post" action="php/agregarpartido.php">
                                    <div class="form-group">
                                        <label for="status">Status:</label>
                                        <input type="text" class="form-control" name="status" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="fecha">Fecha:</label>
                                        <input type="text" class="form-control" name="fecha" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="hora">Hora:</label>
                                        <input type="text" class="form-control" name="hora" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="titulo">Titulo:</label>
                                        <input type="text" class="form-control" name="titulo" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="descripcion">Descripcion:</label>
                                        <input type="text" class="form-control" name="descripcion" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="nombre">Nombre club:</label>
                                        <input type="text" class="form-control" name="nombreClub" required>
                                    </div>

                                    <button type="submit" class="btn btn-default">Agregar</button>
                                </form>
                            </div>

                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->


                <?php include "php/tablapartido.php"; ?>
            </div>
        </div>
    </div>

    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>